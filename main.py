from selenium import webdriver
from selenium.webdriver import ActionChains

browser = webdriver.Firefox()


def perform_UI_test():
    browser.get("https://www.pornhub.com")

    age_button = browser.find_element_by_xpath("/html/body/div[11]/div/div/button")
    age_button.click()

    # Assert lang is English
    home_link_title = browser.find_element_by_xpath(
        "/html/body/div[6]/header/div[2]/div/div/ul/li[1]/a/span"
    ).text
    assert home_link_title == "HOME"

    element_to_hover_over = browser.find_element_by_xpath("/html/body/div[1]/ul/li[9]")
    hover = ActionChains(browser).move_to_element(element_to_hover_over)
    hover.perform()

    ru_lang_item = browser.find_element_by_xpath(
        "/html/body/div[1]/ul/li[9]/ul/li[7]/a"
    )
    ru_lang_item.click()

    # Assert lang is Russian
    home_link_title = browser.find_element_by_xpath(
        "/html/body/div[6]/header/div[2]/div/div/ul/li[1]/a/span"
    ).text
    assert home_link_title == "ГЛАВНАЯ"

    browser.close()


if __name__ == "__main__":
    perform_UI_test()
